/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/of_platform.h>
#include <linux/mod_devicetable.h>
#include <linux/of.h>
#include <linux/io.h>
#include <linux/gpio.h>

#include <linux/gpio/driver.h>
#include <linux/of_address.h>
#include <linux/interrupt.h>

#include <dct_gpio.h>

#define DCT_GPIO_VERSION_ID 0x00000000
#define DCT_GPIO_FILT_SELA 0x00000004
#define DCT_GPIO_FILT_SELB 0x00000008
#define DCT_GPIO_FILT_SELC 0x0000000C
#define DCT_GPIO_FILT_SELD 0x00000010
#define DCT_GPIO_TRIGGER_SEL 0x00000014
#define DCT_GPIO_OE 0x00000100
#define DCT_GPIO_DOUT 0x00000104
#define DCT_GPIO_DSR 0x00000108
#define DCT_GPIO_DCR 0x0000010C
#define DCT_GPIO_DIN 0x00000110
#define DCT_GPIO_IMSR 0x00000114
#define DCT_GPIO_RIS 0x00000118
#define DCT_GPIO_MIS 0x0000011C
#define DCT_GPIO_ICR 0x00000120
#define DCT_GPIO_ISR 0x00000124
#define DCT_GPIO_IRQ_CFG 0x00000128
#define DCT_GPIO_IRQ_CFG_EDGE_LVL 0x0000012C


struct gpio_driver_data {
	struct gpio_chip gc;
	struct device *dev;
	void __iomem *base;
	struct platform_device *pdev;
	spinlock_t lock;
	u32 ngpios;
	u32 offset;
	int irq_parent;
};

static struct platform_driver gpio_driver;

static int dct_gio_get(struct gpio_chip *gc, unsigned off)
{
	struct gpio_driver_data *p = gpiochip_get_data(gc);
	u32 val;

	val = ioread32(p->base + DCT_GPIO_DIN);

	return !!(val & BIT(off));
}

static void dct_gio_set(struct gpio_chip *gc, unsigned off, int val)
{
	struct gpio_driver_data *p = gpiochip_get_data(gc);

	if (val)
		iowrite32(BIT(off), p->base + DCT_GPIO_DSR);
	else
		iowrite32(BIT(off), p->base + DCT_GPIO_DCR);
}

static int dct_gpio_bit_enable(struct gpio_driver_data *p, unsigned reg,
			       unsigned off, bool out)
{
	u32 val;

	spin_lock(&p->lock);

	val = ioread32(p->base + reg);
	if (out)
		val |= BIT(off);
	else
		val &= ~BIT(off);

	iowrite32(val, p->base + reg);

	spin_unlock(&p->lock);

	return 0;
}

static int dct_gio_direction_input(struct gpio_chip *gc, unsigned off)
{
	struct gpio_driver_data *p = gpiochip_get_data(gc);

	dct_gpio_bit_enable(p, DCT_GPIO_OE, off, false);

	return 0;
}

static int dct_gio_direction_output(struct gpio_chip *gc, unsigned off, int val)
{
	struct gpio_driver_data *p = gpiochip_get_data(gc);

	dct_gio_set(gc, off, val);
	dct_gpio_bit_enable(p, DCT_GPIO_OE, off, true);

	return 0;
}

static int dct_gpio_get_direction(struct gpio_chip *gc, unsigned off)
{
	struct gpio_driver_data *p = gpiochip_get_data(gc);
	u32 val;

	val = ioread32(p->base + DCT_GPIO_OE);

	return (val & BIT(off)) ? GPIOF_DIR_OUT : GPIOF_DIR_IN;
}

static void dct_gio_irq_clear(struct irq_data *d)
{
	struct gpio_driver_data *p = irq_data_get_irq_chip_data(d);
	dct_gpio_bit_enable(p, DCT_GPIO_ICR, irqd_to_hwirq(d), true);
}

static void dct_gio_irq_disable(struct irq_data *d)
{
	struct gpio_driver_data *p = irq_data_get_irq_chip_data(d);
	dct_gpio_bit_enable(p, DCT_GPIO_IMSR, irqd_to_hwirq(d), false);
}

static void dct_gio_irq_enable(struct irq_data *d)
{
	struct gpio_driver_data *p = irq_data_get_irq_chip_data(d);
	dct_gpio_bit_enable(p, DCT_GPIO_ICR, irqd_to_hwirq(d), true);
	dct_gpio_bit_enable(p, DCT_GPIO_IMSR, irqd_to_hwirq(d), true);
}

static int dct_gio_irq_set_type(struct irq_data *d, unsigned int type)
{
	struct gpio_driver_data *p = irq_data_get_irq_chip_data(d);
	unsigned int idx = irqd_to_hwirq(d);

	switch (type)
	{
		case IRQ_TYPE_EDGE_RISING:
			dct_gpio_bit_enable(p, DCT_GPIO_IRQ_CFG, idx, true);
			dct_gpio_bit_enable(p, DCT_GPIO_IRQ_CFG_EDGE_LVL, idx, true);
			break;
		case IRQ_TYPE_EDGE_FALLING:
			dct_gpio_bit_enable(p, DCT_GPIO_IRQ_CFG, idx, false);
			dct_gpio_bit_enable(p, DCT_GPIO_IRQ_CFG_EDGE_LVL, idx, true);
			break;
		case IRQ_TYPE_LEVEL_HIGH:
			dct_gpio_bit_enable(p, DCT_GPIO_IRQ_CFG, idx, true);
			dct_gpio_bit_enable(p, DCT_GPIO_IRQ_CFG_EDGE_LVL, idx, false);
			break;
		case IRQ_TYPE_LEVEL_LOW:
			dct_gpio_bit_enable(p, DCT_GPIO_IRQ_CFG, idx, false);
			dct_gpio_bit_enable(p, DCT_GPIO_IRQ_CFG_EDGE_LVL, idx, false);
			break;
		default: return -EINVAL;
	}
	return 0;
}


int disable_gpio_irqs(struct gpio_chip *gc)
{
	struct gpio_driver_data *p = gpiochip_get_data(gc);
	// mask all irqs
	iowrite32(0, p->base + DCT_GPIO_IMSR);
	// clear all irqs
	iowrite32(0xffffFFFF, p->base + DCT_GPIO_ICR);
	return 0;
}

static const struct gpio_chip template_chip = {
	.label = "dct-gpio",
	.owner = THIS_MODULE,
	.get = dct_gio_get,
	.set = dct_gio_set,
	.direction_output = dct_gio_direction_output,
	.direction_input = dct_gio_direction_input,
	.get_direction = dct_gpio_get_direction,
	.can_sleep = false,
	.base = -1,
};

static const struct irq_chip template_irqchip = {
	.name = "dct-gpio-irq",
	.irq_ack = dct_gio_irq_clear,
	.irq_mask = dct_gio_irq_disable,
	.irq_unmask = dct_gio_irq_enable,
	.irq_set_type = dct_gio_irq_set_type,
	.flags = IRQCHIP_IMMUTABLE,
	GPIOCHIP_IRQ_RESOURCE_HELPERS,
};

static void dct_gpio_irq_handler(struct irq_desc *desc)
{
	struct gpio_chip *chip = irq_desc_get_handler_data(desc);
	struct gpio_driver_data *p = gpiochip_get_data(chip);
	struct irq_chip *irqchip = irq_desc_get_chip(desc);
	unsigned long status;
	int hwirq;

	chained_irq_enter(irqchip, desc);

	status = ioread32(p->base + DCT_GPIO_MIS);

	for_each_set_bit(hwirq, &status, chip->ngpio)
		generic_handle_domain_irq(chip->irq.domain, hwirq);

	chained_irq_exit(irqchip, desc);
}


static int dct_gpio_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct device_node *np = dev->of_node;
	struct gpio_driver_data *driver_data;
	struct gpio_chip *gc;
	struct gpio_irq_chip *gi;
	struct resource *res;
	const char *name = dev_name(dev);
	u32 ngpios = 32;
	u32 offset = 0;
	u32 gpio_base = -1;
	int ret;

	driver_data = devm_kzalloc(dev, sizeof(*driver_data), GFP_KERNEL);
	if (!driver_data) {
		dev_err(dev, "devm_kzalloc for driver_data failed\n");
		return -ENOMEM;
	}

	driver_data->dev = dev;
	driver_data->pdev = pdev;

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!res) {
		dev_err(dev, "platform_get_resource failed\n");
		return -ENODEV;
	}

	driver_data->base = devm_ioremap_resource(dev, res);
	if (IS_ERR(driver_data->base)) {
		dev_err(dev, "devm_ioremap_resource %llx %llx failed\n",
			res->start, res->end);
		return PTR_ERR(driver_data->base);
	}

	of_property_read_u32(np, "ngpios", &ngpios);
	of_property_read_u32(np, "offset", &offset);
	of_property_read_u32(np, "gpio-base", &gpio_base);

	driver_data->ngpios = ngpios;
	driver_data->base += offset;
	driver_data->irq_parent = platform_get_irq(pdev, 0);

	spin_lock_init(&driver_data->lock);

	gc = &driver_data->gc;

	// setup gpio_chip
	*gc = template_chip;
	gc->ngpio = ngpios;
	gc->parent = dev;
	gc->label = name;
	gc->base = -1;

	// setup gpio_irq_chip
	gi = &gc->irq;
	gpio_irq_chip_set_chip(gi, &template_irqchip);
	gi->init_hw = disable_gpio_irqs;
	gi->default_type = IRQ_TYPE_NONE;
	gi->handler = handle_level_irq;
	gi->num_parents = 1;
	gi->parents = devm_kcalloc(dev, 1, sizeof(unsigned int), GFP_KERNEL);
	gi->parents[0] = driver_data->irq_parent;
	gi->parent_handler = dct_gpio_irq_handler;

	ret = devm_gpiochip_add_data(dev, gc, driver_data);
	if (ret) {
		dev_err(dev, "failed to register GPIO %s\n", name);
		return ret;
	}

	platform_set_drvdata(pdev, driver_data);

	return 0;
}

static int dct_gpio_remove(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct gpio_driver_data *driver_data = platform_get_drvdata(pdev);

	if (driver_data != NULL) {
		disable_gpio_irqs(&driver_data->gc);
		gpiochip_remove(&driver_data->gc);
		devm_iounmap(dev, driver_data->base);
		devm_kfree(dev, driver_data);
	}

	platform_set_drvdata(pdev, NULL);

	return 0;
}

static const struct of_device_id gpio_match[] = {
	{ .compatible = "dct,dct-gpio", .data = 0 },
	{ /* sentinel */ }
};
MODULE_DEVICE_TABLE(of, gpio_match);

static struct platform_driver gpio_driver = {
	.driver = {
		.name = "dct-gpio",
		.probe_type = PROBE_PREFER_ASYNCHRONOUS,
		.of_match_table = gpio_match,
	},
	.probe = dct_gpio_probe,
	.remove = dct_gpio_remove,
};

static int __init gpio_init_module(void)
{
	int ret;

	ret = platform_driver_register(&gpio_driver);
	if (ret)
		return ret;

	return ret;
}

static void __exit gpio_exit_module(void)
{
	platform_driver_unregister(&gpio_driver);
}

module_init(gpio_init_module);
module_exit(gpio_exit_module);

MODULE_DESCRIPTION("zukimo gpio driver");
MODULE_LICENSE("GPL");
